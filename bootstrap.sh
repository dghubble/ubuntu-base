#!/usr/bin/env bash

# apt-get update
# apt-get install -y apache2
# rm -rf /var/www
# ln -fs /vagrant /var/www


#sudowget

sudo apt-get -y update
sudo apt-get install -y build-essential
sudo apt-get install python-dev

sudo apt-get install -y git
git clone git@bitbucket.org:dghubble/go-pkg-template.git

sudo apt-get install -y python-pip
sudo pip install virtualenv
sudo pip install virtualenvwrapper
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ${HOME}/.profile
# Source manually initially so we can use the same shell session.
source "/usr/local/bin/virtualenvwrapper.sh"

mkvirtualenv ansible
pip install ansible


